package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.Foo;
import org.junit.Assert;
import org.junit.Test;


public class StringTest
{
    @Test
    public void sizeStringTest()
    {
        String length = "cinco";
        int counter = length.length();

        Assert.assertTrue(counter==5);
    }

    @Test
    public void upperCaseConvert()
    {
        String text = "cinco";
        String testUpper = text.toUpperCase();

        Assert.assertEquals(testUpper, "CINCO");
    }

    @Test
    public void convertirAMayusculaNoFalla()
    {
        String text = "cinco";
        String testUpper = text.toUpperCase();

        Assert.assertNotEquals(testUpper, "cinco");
    }

    @Test
    public void concatenarALFinal()
    {
        String firstText = "cinco";
        String secondText = "mentarios";
        String result = firstText.concat(secondText);

        Assert.assertEquals(result, "cincomentarios");
    }

    @Test
    public void concatenarCincoCONCOMENTRIONOEsIguslACinCo()
    {
        String firstText = "cinco";
        String secondText = "mentarios";
        String result = firstText.concat(secondText);

        Assert.assertNotEquals(result, "cinco");
    }


}
