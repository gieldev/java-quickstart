package ar.edu.untref.aydoo.unit;

import org.junit.Assert;
import org.junit.Test;

public class calculadoratest {

    @Test
    public void sumarCaso1(){
        Calculadora calculadora = new Calculadora();
        Integer result = calculadora.sumar(1, 2);
        Assert.assertEquals(result.intValue(), 3);
    }

    public void sumarCaso2(){
        Calculadora calculadora = new Calculadora();
        Integer result = calculadora.sumar(2, 2);
        Assert.assertNotEquals(result.intValue(), 4);
    }

}
